#!/usr/bin/python
#Author: Navdeep Singh
#Purpose: To get the repot for the downtime of service
import getopt, sys, os
import zipfile
import time
import datetime


#LOG_FILE = "/tmp/testlog"
LOG_FILE = "/var/log/auth0.log"
#START_DATE = "1507948983"
#END_DATE = "1507949069"
OUT_FILE = "/tmp/workdata"
#OUT_FILE1 = "/tmp/workdata1"

#fd = open(LOG_FILE, "rt")
out_fd = open(OUT_FILE, 'w')
out_fd.truncate()
out_fd.close()

def prepare_data(START_DATE,END_DATE):
# print START_DATE + END_DATE
 with open(LOG_FILE) as input_data:
    # Skips text before the beginning of the interesting block:
    for line in input_data:
	#print line
        if line.startswith(START_DATE):  # Or whatever test is needed
            break
    # Reads text until the end of the block:
    for line in input_data:  # This keeps reading the file
        if line.startswith(END_DATE):
            break
	out_fd = open(OUT_FILE, 'a+')
        #print line.strip() 
	#out_fd.write(line.strip())

	out_fd.write(line)
	out_fd.close()
 return

def get_report():
 DOWN_FLAG = 0
 DOWN_TIME_START_LIST = []
 UP_TIME_START_LIST = []
 
 print "Getting report for the called duration"
 with open(OUT_FILE) as input_data:
  DOWN_FLAG = 0
  for line in input_data:
     #print line
     #print line.split()[0], line.split()[-1]
     TME = line.split()[0]
     STAT = line.split()[-1]	 
     if STAT != '200' and DOWN_FLAG == 0:
        DOWN_FLAG = 1
        DOWN_TIME_START_LIST.append(TME) 
     if DOWN_FLAG == 1 and STAT == '200':
        DOWN_FLAG = 0
	UP_TIME_START_LIST.append(TME)
        #INDEX += 1
  print 'DownTime_START \t\t\t DownTime_END\t\t\tDuration(in Seconds)'
  for s,e in zip(DOWN_TIME_START_LIST,UP_TIME_START_LIST):
     #Convert Epoch to understandable Format
     a = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(s)))
     b = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(e)))
     #b = time.strftime("%Z - %Y/%m/%d, %H:%M:%S", time.localtime(1507949059))
     
     print a + '\t\t' + b + '\t\t' + str(float(e) - float(s))

 return

#prepare_data();
#get_report();

def usage():
    usage = """
    -h --help                   Prints this
    -s --startdate              Start Date in Format -> YYYY-MM-DD Eg: 2017-10-13
    -e --enddate (argument      End Date in Format -> YYYY-MM-DD Eg: 2017-10-14
    """
    print usage

def main(argv):
    try:
        opt, args = getopt.getopt(argv, "hs:e:", ["help", "startdate=", "enddate="])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)
    for o, a in opt:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-s", "--startdate"):
            #print str(a)
            pattern = '%Y-%m-%d %H:%M:%S'
            epoch_d1 = int(time.mktime(time.strptime(a, pattern)))
	    #print epoch_d1
        elif o in ("-e", "--enddate"):
            #print str(a)
            pattern = '%Y-%m-%d %H:%M:%S'
            epoch_d2 = int(time.mktime(time.strptime(a, pattern)))
            #print epoch_d2
        else:
            assert False, "unhandled option"
    if epoch_d1 < epoch_d2:
      prepare_data(str(epoch_d1),str(epoch_d2))
    else:
      prepare_data(str(epoch_d2),str(epoch_d1))
    get_report()



if __name__ == "__main__":
    main(sys.argv[1:]) # [1:] slices off the first argument which is the name of the program

#getreport()
