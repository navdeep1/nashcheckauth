The logs get generated using the following script


Log Generator Service - nashcheckauth

Run it as systemd service
```
$sudo systemctl start nashcheckauth.service



$sudo systemctl status nashcheckauth.service
● nashcheckauth.service - nashcheckauth
   Loaded: loaded (/etc/systemd/system/nashcheckauth.service; enabled; vendor preset: enabled)
   Active: active (running) since Sun 2017-10-15 03:46:47 UTC; 44s ago
 Main PID: 29694 (nashcheckauth)

```

Manual run the service as following
```
Usage: nashcheckauth -s <ServerIP> -i <Frequency/Interval> -l <absolute/path/of/logfilelocation>
Example:  nashcheckauth -s localhost -i 5s -l /tmp/auth0.log

$ ./nashcheckauth -s localhost -i 5s -l /tmp/auth0.log &
Please tail the log file at /tmp/auth0.log

$ tail -f /tmp/auth0.log
1507952045 osboxes 08:00:27:2e:29:89 200
1507952050 osboxes 08:00:27:2e:29:89 200
1507952055 osboxes 08:00:27:2e:29:89 200
1507952060 osboxes 08:00:27:2e:29:89 200
1507952065 osboxes 08:00:27:2e:29:89 200



```

Cli tool for report generation: nashcli.py

```
    $ ./nashcli.py -h

        -h --help                   Prints this
        -s --startdate              Start Date in Format -> YYYY-MM-DD hh:mm:ss,  Eg: 2017-10-13 03:19:42
        -e --enddate (argument      End Date in Format -> YYYY-MM-DD hh:mm:ss, Eg: 2017-10-14 13:28:30

    $ ./nashcli.py -s "2017-10-14 03:19:42" -e "2017-10-14 13:28:30"
    Getting report for the called duration
    DownTime_START                       DownTime_END                   Duration(in Seconds)
    2017-10-14 03:21:07         2017-10-14 03:21:37             30.0
    2017-10-14 03:29:35         2017-10-14 03:29:50             15.0
    2017-10-14 03:43:18         2017-10-14 03:44:19             61.0
```
Work in progress:

1. Write the service file to make this as a systemd service, so the log generation keeps running in as deamon and restarts on crash - Done
2. Cli tool for report generation - Done
3. Improvements in tools - To add archiving/ziping of log  functionality - Done
